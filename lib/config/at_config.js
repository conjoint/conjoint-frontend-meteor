// Options
AccountsTemplates.configure({
    //defaultLayout: 'emptyLayout',
    showForgotPasswordLink: true,
    overrideLoginErrors: true,
    enablePasswordChange: true,
    sendVerificationEmail: true,

    //enforceEmailVerification: true,
    confirmPassword: false,
    continuousValidation: true,
    //displayFormLabels: true,
    //forbidClientAccountCreation: false,
    //formValidationFeedback: true,
    //homeRoutePath: '/',
    //showAddRemoveServices: false,
    //showPlaceholders: true,

    negativeValidation: true,
    positiveValidation:true,
    negativeFeedback: false,
    positiveFeedback:true,

    // Privacy Policy and Terms of Use
    //privacyUrl: 'privacy',
    //termsUrl: 'terms-of-use',
    // Hooks
    //onLogoutHook: myLogoutFunc,
    //onSubmitHook: mySubmitFunc,
    //preSignUpHook: myPreSubmitFunc,
    postSignUpHook: function(userId, info){
      //insert samples
      JSON.parse(Assets.getText("mockdata.json")).surveydefinitions.forEach(function (doc) {
        doc['createdBy'] = userId;
        Meteor.call('insertDefinition', doc);
    });
    }

});

AccountsTemplates.addField({
    _id: 'firstname',
    type: 'text',
    displayName: "First Name",
    required: true,
    minLength: 2,
    re: /^[a-z\u00C0-\u02AB'´` -]+$/i,
    errStr: 'Name contains unexpected characters'
});

AccountsTemplates.addField({
    _id: 'surname',
    type: 'text',
    displayName: "Surname",
    required: true,
    minLength: 2,
    re: /^[a-z\u00C0-\u02AB'´` -]+$/i,
    errStr: 'Name contains unexpected characters'
});
