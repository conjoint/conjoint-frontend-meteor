// last seems to be the standard
Router.configure({
    layoutTemplate: 'surveyLayout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'pageNotFound'
});

Router.configure({
    layoutTemplate: 'masterLayout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'pageNotFound',
    yieldTemplates: {
        nav: {to: 'nav'},
        footer: {to: 'footer'}
    }
});



Router.map(function() {
    this.route('home', {
        path: '/',
        //layoutTemplate: 'masterLayout'
    });

    this.route('survey-definitions', {layoutTemplate: 'masterLayout'});
    this.route('analyzed-surveys');
    this.route('survey-definitions/edit', {
      name: 'definition.new',
      template:'survey-definitions/edit'
    });

    this.route('survey-definitions/edit/:_id', {
      name: 'definition.edit',
      template:'survey-definitions/edit',
      data: function(){
        var currentId = this.params._id;
        return {surveyDefinition: SurveyDefinitions.findOne({_id: currentId})};
      }
    });

    this.route('survey/:id', {
      action: function () {
        var sessionId= ShortId.generate();
        Meteor.call('createSurveySession', this.params.id, sessionId);
        var params={};
        params['_id']= this.params.id;
        console.log(sessionId);
        params['_sessionId']= sessionId;
        params['page']=1;
        Router.go('survey', params);
      }
    });

    this.route('survey/:_id/:_sessionId/:page',{
      name: 'survey',
      template:'survey',
      layoutTemplate: 'surveyLayout',
      data: function(){
        var currentId = this.params._id;
        return {
          _id: currentId,
          page: this.params.page,
          sessionId: this.params._sessionId,
          survey: Surveys.findOne({surveyId: currentId, sessionId: this.params._sessionId})
      };
      }

    });
});

Router.plugin('ensureSignedIn', {
  except: ['survey', 'home']
});

//Routes
AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('enrollAccount');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('signIn', {redirect: '/survey-definitions'});
AccountsTemplates.configureRoute('signUp', {redirect: '/survey-definitions'});
AccountsTemplates.configureRoute('verifyEmail');
