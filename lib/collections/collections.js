SurveyDefinitions = new Mongo.Collection('SurveyDefinitions');
SurveyDefinitions.publicFields = {
  name: 1,
  description: 1,
  status: 1,
  features:1,
  modification_date:1
};

SurveyDesigns = new Mongo.Collection('SurveyDesigns');

SurveySessions = new Mongo.Collection('SurveySessions');

Surveys = new Mongo.Collection('Surveys');

SurveyResults = new Mongo.Collection('SurveyResults'); //"row__meta","responseId__meta","questionId__meta","alternative__meta"
Feature= new SimpleSchema({
  name: {type:String},
  value: {type:String},
  type: {type:String,
         allowedValues: ["CATEGORICAL", "NUMERICAL"],
         defaultValue: "CATEGORICAL"
       }
});
SurveyResults.schema = new SimpleSchema({
  surveyId: {type: String},
  ownerId: {type:String},
  responseId: {type: String},
  questionId: {type: Number},
  alternativeId: {type: Number},
  features: {type: [Feature]},
  chosen: {type: String,
          allowedValues: ["AS_BEST", "AS_WORST", "NOT_CHOSEN"],
          defaultValue: "NOT_CHOSEN"
  }
});
SurveyResults.attachSchema(SurveyResults.schema);
if (Meteor.isServer){
  SurveyResults._ensureIndex( { "ownerId":1, "surveyId":1, "responseId":1, "questionId":1, "alternativeId":1}, { unique: true } );
  SurveyResults._ensureIndex( {  "surveyId":1, "responseId":1, "questionId":1}, { unique: false } );
}

AnalysisResults = new Mongo.Collection('AnalysisResults');
AnalysisResults.schema = new SimpleSchema({
  surveyId: {type: String},
  ownerId: {type:String},
  modification_date: {type:String},
  name: {type:String},
  description: {type:String},
  willingnessToPay: {type:String}
});
AnalysisResults.attachSchema(AnalysisResults.schema);
