Meteor.methods({
  changeSurveyDescription: function({id, name}){
    const schema = new SimpleSchema({
      id:  { type: String },
      name:  { type: String }
    }).validate({id, name});
    const data ={};
    data['description']=name;
    return SurveyDefinitions.update({_id: id}, {$set: data});
  },
  changeSurveyName: function({id, name}){
    const schema = new SimpleSchema({
      id:  { type: String },
      name:  { type: String }
    }).validate({id, name});
    const data ={};
    data['name']=name;
    return SurveyDefinitions.update({_id: id}, {$set: data});
  },
  removeDefinition: function(id){
    return SurveyDefinitions.remove({_id: id});
  },
  updateAttribute: function({id, key, value}){
    new SimpleSchema({
      id: { type: String },
      key: { type: String },
      value: { type: String}
    }).validate({id, key, value});

    const data ={};
    data[key]=value;
    return SurveyDefinitions.update({_id: id}, {$set: data});
  },
  updateFeatureName: function({id, key, value}){
    new SimpleSchema({
      id: { type: String },
      key: { type: String },
      value: { type: String}
    }).validate({id, key, value});

    const data ={};
    data[key]=value;
    return SurveyDefinitions.update({_id: id}, {$set: data});
  },
  updateLastChanged: function(id){
    var data = {};
    data['_id'] = id;
    SurveyDefinitions.update(data, {$set: {"modification_date" : Date.now()}});
  },
  tooglePublished: function(id){
    var data = {};
    data['_id'] = id;
    var s = SurveyDefinitions.findOne(data);
    var statii= new Set(s.status);
    if (statii.has("UNPUBLISHED"))
      Meteor.call("publish", id);
    else
      Meteor.call("unpublish", id);
  },
  publish: function(id) {
    var data = {};
    data['_id'] = id;
    data['status'] = 'UNPUBLISHED';
    SurveyDefinitions.update(data, {$set : {status : ["PUBLISHED"]}}); //TODO: to maintain multiple states exchange with status.$
  },
  unpublish: function(id) {
    var data = {};
    data['_id'] = id;
    data['status'] = 'PUBLISHED';
    SurveyDefinitions.update(data, {$set : {status : ["UNPUBLISHED"]}}); //TODO: to maintain multiple states exchange with status.$
  },
  initSurveyDefinition: function(){
    var id = SurveyDefinitions.insert({
       createdBy: Meteor.userId(),
       status: ["UNPUBLISHED"],
       features:[
         {name:"", attributes:[], "type":["CATEGORICAL"]},
         {name:"", attributes:[], "type":["CATEGORICAL"]},
         {name:"", attributes:[], "type":["NUMERICAL"]}
       ],
       creation_date: Date.now(),
       modification_date: Date.now()}
     );
    return id;
  },
  insertDefinition: function(doc){
    doc['creation_date']= Date.now();
    doc['modification_date']= Date.now();
    SurveyDefinitions.insert(doc);
  },
  createSurveySession: function(id, sessionId){
    var data = {};
    data['surveyId'] = id;
    data['sessionId'] = sessionId;
    data['createdBy']= Meteor.userId();
    SurveySessions.insert(data);
  },
  insertSurvey: function(data){
    console.log("insert"+data);
    const id = Surveys.insert(data);
    return id;
  },
  upsertChoice: function({pid, cid, sessionId, surveyId}){
    new SimpleSchema({
      pid: { type: Number },
      cid: { type: Number },
      sessionId: { type: String},
      surveyId: { type: String}
    }).validate({pid, cid, sessionId, surveyId});
    const what={};
    what['surveyId']=surveyId;
    what['sessionId']=sessionId;
    const how={};
    how['questionaire.'+pid.toString()+'.bestAlternative']=cid;
    Surveys.update(what, {$set:how},{multi: false, upsert: false},function(post){});

    const question  = Surveys.findOne(what).questionaire[pid];

    const definition = SurveyDefinitions.findOne(surveyId);
    const typeMap={};
    for(f of definition.features){
      typeMap[f.name]= f.type[0];
    }
    console.log(typeMap);

    responses=[]
    for(let altId=0; altId < question.alternatives.length; altId++){
      const response ={};
      response['surveyId']= surveyId;
      //response['ownerId']= Session.getUser();
      response['responseId']= sessionId;
      response['questionId']= pid;
      response['features']=[];
      response['alternativeId']= altId;
      const alt = question.alternatives[altId];
      for (let f in alt){
        const feature = {};
        feature['name']= f;
        feature['value']= alt[f];
        feature['type']= typeMap[f];
        response['features'].push(feature);
      }
      response['chosen']= altId === cid ? "AS_BEST" : "NOT_CHOSEN";
      response['ownerId']=definition.createdBy;
      responses.push(response);
    }
    removeResponses(surveyId, sessionId, pid);
    for(let r of responses){
      SurveyResults.insert(r);
    }

  },
  insertCsv: function(entry){
    var data = entry.data;
    for (var d of data) {

        //SurveyResults.insert(d);
    }

  }
})

removeResponses = function(surveyId, sessionId, pid){
  const what={};
  what['surveyId']=surveyId;
  what['responseId']=sessionId;
  what['questionId']=pid;
  SurveyResults.remove(what);
}
