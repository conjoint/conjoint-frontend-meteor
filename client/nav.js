Template.nav.helpers({
  username: function () {
    return Meteor.user() && Meteor.user().username;
  },
  count: function(expectedStatus){
      if(expectedStatus == "survey")
        return SurveyDefinitions.find().count();
      return AnalysisResults.find().count();
  }
});

Template.user.helpers({
  firstName: function() {
    return Meteor.user().profile.firstname;
  }
});
