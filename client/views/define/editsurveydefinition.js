
Template.surveyDefinitionsEditRow.helpers({
  featureattribute: function(index){
    if (this.attributes === null) return "";
    return this.attributes[index] || "";
  },
  isCategorical: function(){
    return "CATEGORICAL".localeCompare(this.type)===0;
  }
});

Template.surveyDefinitionsEditRow.events({
  "change input.feature.name":function(event) {
    masterTemplate=Template.parentData(2);
    var key = "features."+this.rownumber+".name";
    var data = {};
    data['id']= masterTemplate._id;
    data['key']= key;
    data['value']=event.target.value;
    Meteor.call('updateFeatureName', data, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result)
       console.log("finito", result);
    });
  },

  "change input.feature.attribute":function(event) {
    masterTemplate=Template.parentData(2);
    var index = event.target.name;
    index = index.replace("att", "");
    var key = "features."+this.rownumber+".attributes."+index;
    var data = {};
    data['id']= masterTemplate._id;
    data['key']= key
    data['value']=event.target.value;
    Meteor.call('updateAttribute', data, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result)
       console.log("finito", result);
    });
  },

  "change input": function(event){
    masterTemplate=Template.parentData(2);
    Meteor.call("updateLastChanged", {_id: masterTemplate._id}, function(error, result){
      if(error){
        console.log("error", error);
      }
    });
  }

});


Template.surveyDefinitionsEdit.events({
  "click #remove-surveydefinition": function(){
    Meteor.call("removeDefinition", this._id, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){
         console.log("removed definition", result);
      }
    });
    Router.go("survey-definitions");
  },

  "change input[id=name]": function(event, context) {
        Meteor.call('changeSurveyName', {id: this._id, name: event.target.value}, function(error, result){
          if(error){
              console.log("error", error);
          }
        });

  },

  "change input[id=description]":function(event,context) {
        Meteor.call('changeSurveyDescription', {id: this._id, name: event.target.value}, function(error, result){
          if(error){
              console.log("error", error);
          }
        });
  },

  "change input": function(event){
    Meteor.call("updateLastChanged", {_id: this._id}, function(error, result){
      if(error){
        console.log("error", error);
      }
    });
  }

});
