Template.SurveyDefinitions.onCreated(function() {
  this.autorun(() => {
    this.subscribe('SurveyDefinitions.private');
  });
});
Template.SurveyDefinitions.onRendered(function() {
        var clipboard = new Clipboard('.js-copy-link');
});
Template.SurveyDefinitions.helpers({
  inc: function(i){
    return i+1;
  },
  surveydefinitions: function () {
    var result = SurveyDefinitions.find();
    return result;
  },

  describefeatures: function (){
     var result = "";
     for (var i=this.features.length; i--; ){
        result += this.features[i].name;
        result += i>0 ? ", ": ".";
      }
      return result;
   },

   moment: function(dateToPass){
     return moment(dateToPass).fromNow();
   },

   published: function(){
     var statii= new Set(this.status);
     if (statii.has("UNPUBLISHED")){
       return "";
     }
     return "disabled";
   },

   publishedButtonText: function(){
     var statii= new Set(this.status);
     if (statii.has("UNPUBLISHED")){
       return "Publish Survey";
     }
     return "Survey is public";
   },

  url: function(){
    var a = document.createElement('a');
    a.href = this.url;
    var port = a.port;
    return a.hostname+":"+port+"/survey/"+this._id;
  },
});


Template.SurveyDefinitions.events({

     "click #add-surveydefinition": function (event) {
       id = Meteor.call('initSurveyDefinition');
       Router.go('definition.edit', {_id: id});
     },

     "click .publish": function (event) {
       Meteor.call('tooglePublished', this._id);
      // Router.go('published-surveys');
    },

     "click .js-copy-link": function (event, instance){
       $(event.target).addClass('grey');
     }
 });
