Template.PublishedSurveys.helpers({
  url: function(){
    var a = document.createElement('a');
    a.href = this.url;
    var port = a.port;
    return a.hostname+":"+port+"/survey/"+this._id;
  },

  surveys: function(){
    publishedSurveys= SurveyDefinitions.find({status: "PUBLISHED"});
    return publishedSurveys;
  }

});
