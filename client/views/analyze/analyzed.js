
Template.AnalyzedSurveys.helpers({
  moment: function(dateToPass){
    return moment(dateToPass).fromNow();
  },
  surveys: function(){
    var result = AnalysisResults.find();
    return result;
  }
});


Template.upload.events({
  "click label": function(e){
    $('#files').click();
  },

  "change #files": function (e) {
    console.log(" entering files");
    var config ={
      	delimiter: "",	// auto-detect
      	newline: "",	// auto-detect
      	header: true,
      	dynamicTyping: false,
      	preview: 0,
      	encoding: "",
      	worker: false,
      	comments: true,
      	step: undefined,
      	complete: function (entry) {
           //entry['surveyId__meta']=surveyId; var surveyId = ShortId.generate();
           console.log(entry);
           Meteor.call("insertCsv",entry);
        },
      	error: undefined,
      	download: false,
      	skipEmptyLines: true,
      	chunk: undefined,
      	fastMode: undefined,
      	beforeFirstChunk: undefined,
      	withCredentials: undefined
    };

    var files = e.target.files || e.dataTransfer.files;
    for (var i = 0, file; file = files[i]; i++) {
      if (file.type.indexOf("text") == 0) {
        var reader = new FileReader();
        reader.onloadend = function (e) {
          var text = e.target.result;
          Papa.parse(text, config);
          //console.log(all);

        }
        reader.readAsText(file);
      }
    }
  }
})
