
Template.survey.helpers({
  getNext: function(surveyId, sessionId, nextpage, direction){
    var params = {};
    params['_id']= surveyId;
    params['_sessionId']=sessionId;
    var page = parseInt(nextpage);
    var max = this.survey.questionaire.length;
    if (direction === 'next') {page = page + 1;}
    else {page = page-1;}
    if (isNaN(page) || page > max) page = max;
    if (isNaN(page) || page < 1) page = 1;
    params['page']= page;
    return params;
  },
  products: function(page ) {
     var picks =  this.survey.questionaire[page-1].alternatives;
     const selected_item = this.survey.questionaire[page-1].bestAlternative;
     Session.set("selected_item", selected_item);
    // picks.push({});
     return picks;
  },
  progress: function(page){
    var max = this.survey.questionaire.length;
    var result = 100 * page/max;
    return Math.round(result);
  }

});

const productnames=['Product A', 'Product B', 'Product C', 'No Product '];

Template.card.helpers({
  keys: function(product){
    const result= Object.keys(product);
    if (result.length === 0)
      return ["...","Select if you like none","..."];
    return result;
  },
  productname: function(index){
    return productnames[index];
  },
  value: function(key){
    const product =Template.parentData(1).product;
    if (!_.isEmpty(product))
      return product[key];
    if (key!="...")
      return "Reject all alternatives";
    else
       return "...";
  }
});

Template.deck.onRendered(function(){

  this.autorun(() => {
    if (this.subscriptionsReady()) {
       //const whut=Template.instance().$('.active.js-cardselected').closest('.shape');
       //const oldcards= $('.js-cardselected.active').closest('.shape').attr('id');
       //console.log(oldcards);
       const selected_item = "card"+Session.get("selected_item");
       //$('.side.animating').closest('.shape').shape('reset');
       $('.side.js-front.hidden').closest('.shape').filter((i, item) => {return item.id != selected_item}).shape('flip over');
       $('.side.js-back.active').closest('.shape').filter((i, item) => {return item.id != selected_item}).shape('flip over');


       $('div[id='+selected_item+']').shape('flip over');
       console.log(selected_item + " selected");

    }
  });
});

Template.survey.events({
  "click .card": function(event, template){
     const pid= parseInt(this.pagenumber);
     const cid= parseInt(this.cardnumber);
     const data={}
     data['pid']=pid-1;
     data['cid']=cid;
     data['sessionId']=this.sessionid;
     data['surveyId']=this.surveyid;
     Meteor.call('upsertChoice', data);
     //$('#card'+Session.get("selected_item")).shape('flip over');
     Session.set("selected_item", cid);
  }
});
