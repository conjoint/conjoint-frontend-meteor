docker run -d \
    -e ROOT_URL=http://conjoint.it \
    -e MONGO_URL=mongodb://localhost \
    -e MONGO_OPLOG_URL=mongodb://localhost \
    -p 8080:80 \
    rkofler/conjoint
