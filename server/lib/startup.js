switch (process.env.ROOT_URL) {
    case "http://conjoint.it":
       break;
    case "http://localhost:3000/":
       break;
}

Meteor.startup(function () {
  SurveySessions._ensureIndex({sessionId:1, surveyId:1}, {unique: 1});
  Surveys._ensureIndex({sessionId:1, surveyId:1}, {unique: 1});
});
